## [3.2.4](https://gitlab.com/to-be-continuous/helmfile/compare/3.2.3...3.2.4) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([1a0ba55](https://gitlab.com/to-be-continuous/helmfile/commit/1a0ba552a2c28b17a0adbdcabada0e0b2dc2686b))

## [3.2.3](https://gitlab.com/to-be-continuous/helmfile/compare/3.2.2...3.2.3) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([3e548fc](https://gitlab.com/to-be-continuous/helmfile/commit/3e548fc4f240045d8a0216d41a4ef43e5beaf6b5))

## [3.2.2](https://gitlab.com/to-be-continuous/helmfile/compare/3.2.1...3.2.2) (2024-04-02)


### Bug Fixes

* chmod 600 on GitLab-provided Kubeconfig file to prevent warning messages from kubectl ([083295b](https://gitlab.com/to-be-continuous/helmfile/commit/083295b504da475ba99332a9ec89823bdfbedeb2))

## [3.2.1](https://gitlab.com/to-be-continuous/helmfile/compare/3.2.0...3.2.1) (2024-03-20)


### Bug Fixes

* make namespace implicit and overridable ([ed07d6f](https://gitlab.com/to-be-continuous/helmfile/commit/ed07d6f3591e222db401bbe611211268dbe184d2))

# [3.2.0](https://gitlab.com/to-be-continuous/helmfile/compare/3.1.1...3.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([16a24fb](https://gitlab.com/to-be-continuous/helmfile/commit/16a24fb9b09e8ccd58bb9ee9a4e9b0e6a026d347))

## [3.1.1](https://gitlab.com/to-be-continuous/helmfile/compare/3.1.0...3.1.1) (2023-12-17)


### Bug Fixes

* resolve helmfile lint job not running when enabled ([ed04769](https://gitlab.com/to-be-continuous/helmfile/commit/ed047691e3a88cbb0015c92bf5d3780cef171f90))

# [3.1.0](https://gitlab.com/to-be-continuous/helmfile/compare/3.0.3...3.1.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([c97e799](https://gitlab.com/to-be-continuous/helmfile/commit/c97e799c042f0b6a3eec999e27ca2ae49f23eb8c))

## [3.0.3](https://gitlab.com/to-be-continuous/helmfile/compare/3.0.2...3.0.3) (2023-12-2)


### Bug Fixes

* envsubst when variable contains a '&' ([525e8fe](https://gitlab.com/to-be-continuous/helmfile/commit/525e8fe24a6506d5e9a7ea564b9fa44dfb53c5e3))

## [3.0.2](https://gitlab.com/to-be-continuous/helmfile/compare/3.0.1...3.0.2) (2023-11-06)


### Bug Fixes

* **context:** use KUBE_CONTEXT when present ([9b5f59c](https://gitlab.com/to-be-continuous/helmfile/commit/9b5f59c0b4c4cf49b7b2fef5fb1187e908303e9b)), closes [#5](https://gitlab.com/to-be-continuous/helmfile/issues/5)

## [3.0.1](https://gitlab.com/to-be-continuous/helmfile/compare/3.0.0...3.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([2053ab3](https://gitlab.com/to-be-continuous/helmfile/commit/2053ab3ee4850b5f96e90334b6f69e3afb8b7006))

# [3.0.0](https://gitlab.com/to-be-continuous/helmfile/compare/2.1.0...3.0.0) (2023-09-26)


* feat!: support environment auto-stop ([c0e94d7](https://gitlab.com/to-be-continuous/helmfile/commit/c0e94d78182ffb0166c391bed7131f7689e48cf8))


### Features

* add $kube_namespace ctx variable ([7d93ae7](https://gitlab.com/to-be-continuous/helmfile/commit/7d93ae70f7131010c78f33ded577225dbe259235))


### BREAKING CHANGES

* now review environments will auto stop after 4 hours
by default. Configurable (see doc).

# [2.1.0](https://gitlab.com/to-be-continuous/helmfile/compare/2.0.0...2.1.0) (2023-09-02)


### Features

* allow propagate custom output variables ([4408541](https://gitlab.com/to-be-continuous/helmfile/commit/440854107ebae1878bc35c7486309fd855e06d2c))

# [2.0.0](https://gitlab.com/to-be-continuous/helmfile/compare/1.1.0...2.0.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration (see doc) ([ba114d8](https://gitlab.com/to-be-continuous/helmfile/commit/ba114d80e34e3d04c00cffbf14724a709ecb9d69))


### BREAKING CHANGES

* **oidc:** OIDC authentication support now requires explicit configuration (see doc)

# [1.1.0](https://gitlab.com/to-be-continuous/helmfile/compare/1.0.0...1.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([61e7534](https://gitlab.com/to-be-continuous/helmfile/commit/61e753436662bbcc03607f1a614709aff894a0a7))

# 1.0.0 (2023-04-27)


### Features

* initial release ([cef9b8b](https://gitlab.com/to-be-continuous/helmfile/commit/cef9b8be516fe75e22a9391414a83625bea3f6cc))
